<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layout.main');
// });


Route::get('/', 'DataController@master');

Route::get('/content','DataController@index')->name('home');

Route::get('/create ','DataController@create');

Route::post('/posts','DataController@store');

Route::get('/content/{blog}','DataController@show');


Route::post('content/{blog}/comments','CommentsController@store');

Route::get('/register','registerationController@create');
Route::post('/register','registerationController@store');

Route::get('login','SessionsController@show');
Route::post('login','SessionsController@store');

Route::get('logout','SessionsController@destroy');





