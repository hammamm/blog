<?php

namespace App;

use carbon\carbon; 

class blog extends Model
{



	protected $fillable = ['title','body','user_id'];


	public function comments(){


		return $this->hasMany(comment::class);
	}


	public function user(){

    	return $this->belongsTo(user::class);
    }




	public function addComment($body){
   	//    	comment::create([
	// 	'body' => $body,
	// 	'blog_id' => $this->id
	// ]);

   	//we have relation in class so insted of doing these lines we do
		$this->comments()->create(compact('body'));
   }



    public function scopeFillter($query,$fillters)
    {

    	if($month = $fillters['month']){
            $query->whereMonth('created_at',carbon::parse($month)->month );
        }

        if($year = $fillters['year']){
            $query->whereYear('created_at', $year);
        }
    }



   
}
