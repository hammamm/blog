<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\blog;

class DataController extends Controller
{


public function __construct()
{
    $this->middleware('auth')->except(['index','show','master']);
}



    public function index (){

        $blogs = blog::latest()
        ->fillter(request(['month','year']))
        ->get();

        
        // $blogs = $blogs->get();




       $archives = blog::selectRaw('year(created_at) year,monthname(created_at) month , 
count(*) published')
       ->groupBy ('year','month')
       ->orderByRaw('min(created_at) desc')
       ->get()
       ->toArray();


    	return view('layout.content',compact('blogs','archives'));
    }


    public function master (){
    	return view('master');
    }


    public function create(){
    	return view('posts.create');
    }


    public function store( ){

    	$this->validate(request(),[
            'title'=>'required',
            'body'=>'required'
        ]);



        auth()->user()->publish( new blog(request(['title','body'])));



    	return redirect('/content');
    }

    public function show(blog $blog){

        return view('layout.blog',compact('blog'));
    }




    
}
