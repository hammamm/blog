<?php

namespace App\Http\Controllers;

use App\blog;
use App\comment;

class CommentsController extends Controller
{
    public function store (blog $blog){

    	
		//this way to make query and fetch from data
    	// $comment = App\comment::find(id);
    	//	return $post->id;


		//insted of defining an id we use rout model binding
		// $comment = comment::create([
		// 	'body' => request('body'),
		// 	'blog_id' => $blog->id
		// ]);

		$this->validate(request(), ['body'=>'required|min:2']);

		//insted of calling the data here we will call a function from model
		$blog->addComment(request('body'));


       // return redirect('/content/'.$blog->id);
        return back();//this function makes redirect to last page
    }
}
