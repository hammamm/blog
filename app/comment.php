<?php

namespace App;


class comment extends Model
{

	protected $fillable = ['blog_id','body','user_id'];
    
    public function blog(){

    	return $this->belongsTo(blog::class);
    }



    public function user(){

    	return $this->belongsTo(user::class);
}

}
