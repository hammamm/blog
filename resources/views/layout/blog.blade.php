@extends('master')



@section('content')
<div class="col-sm-8 blog-main">
<ul class="list-unstyled">
	<li>
<center><h1><?=  $blog->title ?></h1></center>
</li>
<br>
<li>
<h5>{{$blog->body}}</h5>
</li>
</ul>

<br>
<br>
<br>
<hr>
<center><h4>Comments</h4></center>
<br>
<div class="comments">
	
	<ul class="list-group">
	@foreach ($blog->comments as $comment)
	
	<li class="list-group-item">
		
		{{$comment->created_at->diffForHumans()}}:
		&nbsp;
		

		<strong>
		{{$comment->body}}</li>
</strong>

	


	@endforeach

	@if (!count($blog->comments))
	<li class="list-group-item">No comments yet</li>
	@endif
</ul>

</div>
 <hr>
<br>
<div class="card">
	<div class="card-block">
		<form method="POST" action="/content/{{$blog->id}}/comments">

			{{csrf_field()}}
			<div class="form-group">
				<textarea name="body" placeholder="your comment here" class="form-control"></textarea>
			</div>
			@include('layout.error')



			<div class="form-group">
				<center><button type="submit" class="btn btn-primary">add comment</button></center>
			</div>


		</form>
	</div>
</div>
</div>


@endsection

